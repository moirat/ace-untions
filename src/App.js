import React from 'react';
import Tile from './components/issueTile.js'
import './index.css';
import IssueDetail from './components/IssueDetail.js'


class App extends React.Component {

  state= {
    issues: [{
      route: "ABCD",
      violationType: "L2",
      pilot: {name: "Moira", id: "1234", email: "mcturner@gmail.com"},
      narrative: "Hello my name is you",
      viewerDetails: {name: "Jose", time: "10:30"},
      submitTime: "10:30 pm",
      violations: ["ReRouting"],
      lastView: "Bob 10:30 pm",
      numViews: 2,
      numNotes: 4,
    }, {
      route: "JFKL",
      pilot: {name: "Moira", id: "1234", email: "moiraturner@gmail.com"},
      viewerDetails: {name: "Jose", time: "10:30"},
      submitTime: "1:30 pm",
      violation: ["ReRouting","Hotel"],
      violationType: "L2",
      lastView: "Bob 10:30 pm",
      numViews: 2,
      numNotes: 5,
    }, {
      route: "LMNOP",
      pilot: {name: "Dally", id: "1234", email: "mcturner@gmail.com"},
      viewerDetails: {name: "Jose", time: "10:30"},
      submitTime: "10:30",
      violation: ["ReRouting", "Reserve"],
      violationType: "L2",
      lastView: "Bob 10:30 pm",
      numViews: 4,
      numNotes: 4,
    }]
  }

  render() {
    const issue = this.state.issues[0]


    return(
      <IssueDetail issue={issue}/>
    )
  }

  //   const issues = this.state.issues;

  //   const issuesList = issues.length ? (
  //     issues.map(issue => {
  //       return (
  //         <Tile issue={issue} />
  //       );
  //     })
  //   ) : (
  //     <p>You have no todos</p>
  //   );
  
  //   return <div >{issuesList}</div>;
  // };




}

export default App;
