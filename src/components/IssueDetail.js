import React from 'react';
import '../index.css';
import Default from "./defaultView"

class IssueDetails extends React.Component {
    state = {
        view: "default",
    };


    render(){

    return (
        <div class="container-fluid">
            <div class="col">
                <Default issue ={this.props.issue}/>
            </div>
        
            <div class="col">
                <button>Confirm</button>
                <button>PWA Compliant</button>
                <button>Defer</button>
                <button>Call</button>
                <button>Email</button>
                <button>MPI</button>
                <button>RPH</button>
                <button>Mirror View</button>
                <button>Add Note</button>
                <button>Create New</button>
            </div>
        </div>

    )
    }



}
export default IssueDetails